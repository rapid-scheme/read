;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> This library provides a reader for external representations of
;;> Scheme objects compatible with the R7RS syntax.  It has two
;;> advantages over the standard reader in \code{(scheme read)}.
;;> Firstly, it accurately reports parsing errors by raising a
;;> continuable exception, so that parsing can be continued after
;;> reporting the error.  Secondly, all the parsed datums are provided
;;> with exact source locations by returning the parsed datums wrapped
;;> in syntax objects as defined by the library \code{(rapid syntax)}.

;;; Source ports

;;> \section{Source ports}

;;> A \emph{source port} is a port-like object wrapping a textual
;;> input port that maintains line and column information and a
;;> case-insensitivity flag.

;;; Reader errors

;;> \section{Reader errors}

;;> Readers errors are signaled by raising a continuable exception, a
;;> syntax error object from \code{(rapid syntax)}.  If an exception
;;> handler handling a reader error returns, the reader continues and
;;> tries to parse the next datum.

;;; Procedures

;;> \section{Procedures}

(define-record-type <source-port>
  (%make-source-port port source ci? line column)
  source-port?
  (port source-port-port)
  (source source-port-source)
  (ci? source-port-ci? source-port-set-ci!)
  (line source-port-line source-port-set-line!)
  (column source-port-column source-port-set-column!))

(define (source-port-peek-char source-port)
  (peek-char (source-port-port source-port)))

(define (source-port-read-char source-port)
  (let ((char (read-char (source-port-port source-port))))
    (cond
     ((eof-object? char) char)
     (else
      (case char
	((#\tab)
	 (source-port-set-column! source-port
				  (* (+ (quotient
					 (source-port-column source-port) 8)
					1)
				     8)))
	((#\newline)
	 (source-port-set-column! source-port 0)
	 (source-port-set-line! source-port
				(+ (source-port-line source-port) 1)))
	((#\return)
	 (source-port-set-column! source-port 0))
	(else
	 ;; FIXME: Handle Unicode character width correctly.
	 ;; See also <https://www.gnu.org/prep/standards/standards.html#Errors>.
	 (source-port-set-column! source-port
				  (+ (source-port-column source-port) 1))))
      char))))

;;> \procedure{(make-source-port port source ci?)}

;;> Returns a new source port wrapping the textual input port
;;> \var{port} with position initialized to the start of a text
;;> file (line number 0 and column number 1), and case-insensitivity
;;> set according to \var{ci?}.

(define (make-source-port port source ci?)
  (assume (port? port))
  (assume (string? source))
  (assume (boolean? ci?))
  (%make-source-port port source ci? 1 0))

;;> \procedure{(source-port? obj)}

;;> Returns \scheme{#t} if \var{obj} is a source port, and \scheme{#f}
;;> otherwise.

;;> \procedure{(source-port-set-ci! ci?)}

;;> Sets the case-insensitivity flag of \var{source-port} according to
;;> \var{ci?}.

;;> \procedure{(source-port-fold-case! source-port)}

;;> Sets the case-insensitivity flag of \var{source-port}.

(define (source-port-fold-case! source-port)
  (source-port-set-ci! source-port #t))

;;> \procedure{(source-port-no-fold-case! source-port)}

;;> Clears the case-insensitivity flag of \var{source-port}.

(define (source-port-no-fold-case! source-port)
  (source-port-set-ci! source-port #f))

;;> \procedure{(read-syntax source-port)}

;;> Reads the next datum available at \var{source-port} wrapped in a
;;> syntax object.

(define (read-syntax source-port)

  (define source (source-port-source source-port))
  
  (define (peek) (source-port-peek-char source-port))
  
  (define (read)
    (let ((char (source-port-read-char source-port)))
      (if (eof-object? char)
	  ((current-eof-handler))
	  char)))

  (define (fold-case!) (source-port-fold-case! source-port))

  (define (no-fold-case!) (source-port-no-fold-case! source-port))

  (define (ci?) (source-port-ci? source-port))

  (define (string->identifier string)
    (let ((string (if (ci?) (string-foldcase string) string)))
      (string->symbol string)))
    
  (define (position) (make-position (source-port-line source-port)
				    (source-port-column source-port)))

  (define (reader-error start message . obj*)
    (let*
	((source-location (make-source-location source
						(position-line start)
						(position-column start)
						(source-port-line source-port)
						(source-port-column source-port)))
	 (syntax (make-syntax #f source-location)))
      (apply raise-syntax-error syntax message obj*)))

  (define current-dot-handler
    (make-parameter (lambda (start)
		      (reader-error start "unexpected dot in source"))))

  (define current-closing-parenthesis-handler
    (make-parameter (lambda (start)
		      (reader-error start "unexpected closing parenthesis in source"))))

  (define current-eof-handler
    (make-parameter #f))
  
  (define (check-identifier! start token)
    (string-for-each (lambda (char)
		       (unless (subsequent? char)
			 (reader-error start "unexpected character in identifier ‘~a’"
				       char)))
		     token))
  
  (define labels (mapping label-comparator))
  (define (label-exists? number)
    (mapping-contains? labels number))
  (define (labels-add! number label)
    (set! labels (mapping-set labels number label)))
  (define (labels-ref start number)
    (mapping-ref labels
		 number
		 (lambda ()
		   (reader-error start "unknown label ‘#~a’" number))))

  (define references (mapping syntax-comparator))
  (define (references-add! syntax label)
    (set! references (mapping-set references syntax label)))
  (define (references-ref syntax)
    (mapping-ref/default references syntax #f))
  
  (define (read-syntax)
    (let ((start (position)))
      
      (define (syntax datum) (make-syntax datum (source-location)))
      
      (define (source-location)
	(make-source-location source
			      (position-line start)
			      (position-column start)
			      (source-port-line source-port)
			      (source-port-column source-port)))

      (define (read-abbreviation symbol)
	(let* ((keyword (syntax symbol))
	       (datum (read-syntax)))
	  (syntax (list keyword datum))))
      
      (define (number start token)
	(cond
	 ((string->number token) => syntax)
	 (else
	  (reader-error start "invalid number ‘~a’" token))))
      
      (define (read-identifier start)
	(let ((token (read-token)))
	  (cond
	   ((string->number token) => syntax)
	   (else
	    (check-identifier! start token)
	    (syntax (string->identifier token))))))

      (define (read-peculiar-identifier start)
	(let ((token (read-token)))
	  (cond
	   ((string->number token) => syntax)
	   ((string=? token ".")
	    ((current-dot-handler) start))
	   (else
	    (when (and (>= (string-length token) 2)
		       (or (and (sign? (string-ref token 0))
				(if (char=? (string-ref token 1) #\.)
				    (or (= (string-length token) 2)
					(not (dot-subsequent? (string-ref token 2)))))
				(not (sign-subsequent? (string-ref token 1))))
			   (and (char=? (string-ref token 0) #\.)
				(not (dot-subsequent? (string-ref token 1))))))
	      (reader-error start "invalid peculiar identifier ‘~a’" token))
	    (check-identifier! start token)
	    (syntax (string->identifier token))))))

      (define (read-string start)
	(parameterize ((current-eof-handler
			(lambda ()
			  (reader-error start "unterminated string")
			  #\")))
	  (receive (chars)
	      (let loop ()
		(let ((start (position)))
		  (case (read)
		    ((#\")
		     '())
		    ((#\\)
		     (cond
		      ((read-escape start)
		       => (lambda (char)
			    (cons char (loop))))
		      (else (loop))))
		    ((#\return)
		     (when (eqv? (peek) #\newline)
		       (read))
		     (cons #\newline (loop)))
		    (else => (lambda (char)
			       (cons char (loop)))))))
	    (syntax (list->string chars)))))

      (define (read-symbol start)
	(parameterize ((current-eof-handler
			(lambda ()
			  (reader-error start "unterminated identifier")
			  #\|)))
	  (receive (chars)
	      (let loop ()
		(let ((start (position)))
		  (case (read)
		    ((#\|)
		     '())
		    ((#\\)
		     (cond
		      ((case (peek)
			 ((#\space #\tab #\return #\newline)
			  (read))
			 (else
			  (read-escape start)))
		       => (lambda (char)
			    (cons char (loop))))
		      (else
		       (loop))))
		    (else
		     => (lambda (char)
			  (cons char (loop)))))))
	    (syntax (string->identifier (list->string chars))))))

      (define (read-list start)
	(parameterize ((current-eof-handler
			(lambda ()
			  (reader-error start "unterminated list")
			  ((current-closing-parenthesis-handler) #f))))
	  (receive (elements dot-start)
	      (call/cc
	       (lambda (break)
		 (let loop ((elements '()))
		   (parameterize ((current-closing-parenthesis-handler
				   (lambda (start)
				     (break elements #f)))
				  (current-dot-handler
				   (lambda (start)
				     (break elements start))))
		     (loop (cons (read-syntax) elements))))))
	    (receive (lst)
		(call/cc
		 (lambda (return)
		   (if dot-start
		       (if (null? elements)
			   (reader-error dot-start "unexpected dot in source")
			   (let ((tail (unwrap-tail (read-syntax))))
			     (parameterize ((current-closing-parenthesis-handler
					     (lambda (start)
					       (return (append-reverse elements tail)))))
			       (read-syntax)
			       (reader-error start "expected end of list after dotted tail")
			       ((current-closing-parenthesis-handler) #f))))
		       (reverse elements))))
	      (syntax lst)))))

      (define (read-sharp-syntax start)
	(parameterize ((current-eof-handler
			(lambda ()
			  (reader-error start "incomplete sharp syntax at end of input")
			  ((current-eof-handler)))))
	  (if (eof-object? (peek))
	      (read)
	      (let ((char (char-foldcase (peek))))
		(cond
		 ;; Booleans
		 ((member char '(#\t #\f) char=?)
		  (read-boolean start))
		 ;; Labels
		 ((char<=? #\0 char #\9)
		  (read-label start))
		 (else
		  (read)	
		  (case char
		    ;; Numbers
		    ((#\e #\i #\b #\o #\d #\x)
		     => (lambda (char)
			  (number start (string-append "#"
						       (string char) 
						       (read-token)))))
		    ;; Nested comment
		    ((#\|)
		     (skip-nested-comment! start)
		     #f)
		    ;; Datum comment
		    ((#\;)
		     (parameterize ((current-eof-handler
				     (lambda ()
				       (reader-error start "incomplete datum comment"))))
		       (read-syntax))
		     #f)
		    ;; Directives
		    ((#\!)
		     (read-directive! start)
		     #f)
		    ;; Characters
		    ((#\\)
		     (read-character start))
		    ;; Vector
		    ((#\()
		     (read-vector start))
		    ;; Bytevector
		    ((#\u)
		     (read-bytevector start))
		    (else
		     => (lambda (char)
			  (reader-error start "invalid sharp syntax ‘#~a’" char))))))))))

      (define (read-boolean start)
	(let ((token (read-token)))
	  (case (string->symbol (string-foldcase token))
	    ((t true)
	     (syntax #t))
	    ((f false)
	     (syntax #f))
	    (else
	     (reader-error start "invalid boolean ‘~a’" token)))))      

      (define (read-character start)
	(let*
	    ((char (read))
	     (token (string-append (string char) (read-token))))
	  (cond
	   ((= (string-length token) 1)
	    (syntax (string-ref token 0)))
	   ((hex-scalar-value token)
	    => (lambda (value)
		 (let ((char (code-point->character start value)))
		   (if char
		       (syntax char)
		       char))))
	   (else
	    (case (string->symbol (if (ci?) (string-foldcase token) token))
	      ((alarm) (syntax #\alarm))
	      ((backspace) (syntax #\backspace))
	      ((delete) (syntax #\delete))
	      ((escape) (syntax #\escape))
	      ((newline) (syntax #\newline))
	      ((null) (syntax #\null))
	      ((return) (syntax #\return))
	      ((space) (syntax #\space))
	      ((tab) (syntax #\tab))
	      (else
	       (reader-error start "invalid character name ‘~a’" token)))))))

      (define (read-vector start)
	(receive (datum)
	    (call/cc
	     (lambda (return)
	       (let loop ((elements '()))
		 (define (terminate-vector)
		   (return (list->vector (reverse elements))))
		 (parameterize ((current-closing-parenthesis-handler
				 (lambda (start)
				   (terminate-vector)))
				(current-eof-handler
				 (lambda ()
				   (reader-error start "unterminated vector")
				   (terminate-vector))))
		   (loop (cons (read-syntax) elements))))))
	  (syntax datum)))

      (define (read-bytevector start)
	(cond
	 ((and (char=? (read) #\8) (char=? (read) #\())
	  (receive (datum)
	      (call/cc
	       (lambda (return)
		 (let loop ((bytes '()))
		   (define (terminate-bytevector)
		     (return (apply bytevector (reverse bytes)))       )
		   (parameterize ((current-closing-parenthesis-handler
				   (lambda (start)
				     (terminate-bytevector)))
				  (current-eof-handler
				   (lambda ()
				     (reader-error start "unterminated bytevector"))))
		     (let*
			 ((syntax (read-syntax)) 
			  (datum (unwrap-syntax syntax)))
		       (if (and (exact-integer? datum)
				(<= 0 datum 255))
			   (loop (cons datum bytes))
			   (let*
			       ((location (syntax-location syntax))
				(start (make-position (source-location-start-line location)
						      (source-location-start-column location))))
			     (reader-error start "not a byte"))))))))
	    (syntax datum)))
	 (else
	  (reader-error start "invalid bytevector"))))

      (define (read-number)
	(let loop ((value #f))
	  (if (and (char? (peek)) (char<=? #\0 (peek) #\9))
	      (loop (+ (* 10 (or value 0)) (- (char->integer (read)) #x30)))
	      value)))
      
      (define (read-label start)
	(let ((number (read-number)))
	  (case (read)
	    ((#\=)
	     (when (label-exists? number)
	       (reader-error start "duplicate definition of label ‘#~a’" number))
	     (let ((label (make-label)))
	       (labels-add! number label)
	       (let ((referenced-syntax (read-syntax)))
		 (label-set-syntax! label referenced-syntax)
		 (cond
		  ;; Referenced syntax is a reference itself.
		  ((references-ref referenced-syntax)
		   => (lambda (referenced-label)
			(cond
			 ((eq? referenced-label label)
			  (reader-error start "invalid label self-reference"))
			 (else
			  (label-add-references! referenced-label
						 (label-references label))))))
		  ;; References can be patched
		  (else
		   (let ((datum (unwrap-syntax referenced-syntax)))
		     (label-for-each-reference
		      (lambda (reference)
			(syntax-set-datum! reference datum))
		      label)
		     (label-delete-references! label))))
		 referenced-syntax)))
	    ((#\#)
	     (let ((label (labels-ref start number)))
	       (cond
		((label-syntax label)
		 => (lambda (referenced-syntax)
		      (syntax (unwrap-syntax referenced-syntax))))
		(else
		 (let ((referencing-syntax (syntax #f)))
		   (references-add! referencing-syntax label)
		   (label-add-reference! label referencing-syntax)
		   referencing-syntax)))))	    
	    (else
	     (reader-error start "expected ‘#’ or ‘=’ after label")))))
    
      (cond
       ;; Eof
       ((eof-object? (peek))
	(read))
       ;; Numbers
       ((char<=? #\0 (peek) #\9)
	(number start (read-token)))
       ;; Identifiers
       ((initial? (peek))
	(read-identifier start))
       ;; Peculiar identifiers
       ((or (sign? (peek))
	    (char=? (peek) #\.))
	(read-peculiar-identifier start))
       (else
	(case (read)
	  ;; Skip whitespace
	  ((#\newline #\return #\space #\tab)
	   (read-syntax))
	  ;; Skip line comments
	  ((#\;)
	   (skip-until-new-line!)
	   (read-syntax))
	  ;; Strings
	  ((#\")
	   (read-string start))
	  ;; Identifiers enclosed in vertical lines
	  ((#\|)
	   (read-symbol start))
	  ;; Lists
	  ((#\()
	   (read-list start))
	  ;; Closing parenthesis
	  ((#\))
	   ((current-closing-parenthesis-handler) start))
	  ;; Quote
	  ((#\')
	   (read-abbreviation 'quote))
	  ;; Quasiquote
	  ((#\`)
	   (read-abbreviation 'quasiquote))
	  ;; Unquote
	  ((#\,)
	   (cond
	    ((eqv? (peek) #\@)
	     (read)
	     (read-abbreviation 'unquote-splicing))
	    (else
	     (read-abbreviation 'unquote))))
	  ;; Sharp syntax
	  ((#\#)
	   (read-sharp-syntax start))
	  ;; Invalid character
	  (else
	   => (lambda (char)
		(reader-error start "unexpected character ‘~a’ in input" char))))))))

  (define (read-token)
    (list->string
     (let loop ()
       (cond
	((delimiter? (peek))
	 '())
	(else
	 (let ((char (read)))
	   (cons char (loop))))))))
  
  (define (read-escape start)
    (case (read)
      ((#\a) #\alarm)
      ((#\b) #\backspace)
      ((#\t) #\tab)
      ((#\n) #\newline)
      ((#\r) #\return)
      ((#\") #\")
      ((#\|) #\|)
      ((#\x #\X)
       (cond
	((read-hex-scalar-value)
	 => (lambda (value)
	      (case (read)
		((#\;)
		 (code-point->character start value))
		(else
		 => (lambda (char)
		      (reader-error start "semicolon expected, but found ‘~a’" char)
		      (code-point->character start value))))))
	(else
	 (reader-error start "hex scalar value expected"))))
      ((#\space #\tab #\return #\newline)
       => (lambda (char)
	    (let loop ((char char) (start #f))
	      (case char
		((#\space #\tab)
		 (let ((start (position)))
		   (loop (read) start)))
		((#\return #\newline)
		 => (lambda (char)
		      (when (and (char=? char #\return)
				 (char=? (peek) #\newline))
			(read))
		      (let loop ()
			(case (peek)
			  ((#\space #\tab)
			   (read)
			   (loop))))))
		(else
		 (reader-error start "unexpected character ‘~a’ before line ending"
			       char))))
	    #f))
      (else
       => (lambda (char)
	    char))))

  (define (read-hex-scalar-value)
    (let loop ((value #f))
      (cond
       ((hex-digit (peek))
	=> (lambda (digit)
	     (read)
	     (loop (+ (* 16 (or value 0)) digit))))
       (else
	value))))
  
  (define (code-point->character start value)
    (cond
     ((or (<= 0 value #xD7FF)
	  (<= #xE000 value #x10FFFF))
      (integer->char value))
     (else
      (reader-error start "not a valid unicode code point"))))
  

  (define (read-directive! start)
    (let ((token (read-token)))
      (case (string->symbol (string-foldcase token))
	((fold-case)
	 (fold-case!))
	((no-fold-case)
	 (no-fold-case!))
	(else
	 (reader-error start "invalid directive ‘~a’" token)))))
  
  (define (skip-until-new-line!)
    (let loop ()
      (case (read)
	((#\newline)
	 #f)
	((#\return)
	 (when (eqv? (peek) #\newline)
	   (read)))
	(else
	 (loop)))))

  (define (skip-nested-comment! start)
    (call/cc
     (lambda (return)
       (parameterize ((current-eof-handler
		       (lambda ()
			 (reader-error start "unterminated nested comment")
			 (return #f))))
	 (let loop ((depth 0))
	   (case (read)
	     ((#\#)
	      (if (char=? (read) #\|)
		  (loop (+ depth 1))
		  (loop depth)))
	     ((#\|)
	      (if (char=? (read) #\#)
		  (when (> depth 0)
		    (loop (- depth 1)))
		  (loop depth)))
	     (else
	      (loop depth))))))))
  
  ;; Invoke the reader
  (call/cc
   (lambda (return)
     (parameterize ((current-eof-handler
		     (lambda ()
		       (return (eof-object)))))
       (let loop ()
	 (or (read-syntax) (loop)))))))

(define (delimiter? char)
  (case char
    ((#\space #\tab #\return #\newline #\| #\( #\) #\" #\;)
     #t)
    (else
     (eof-object? char))))

(define (initial? char)
  (or (char<=? #\A char #\Z)
      (char<=? #\a char #\z)
      (case char
	((#\! #\$ #\% #\& #\* #\/ #\: #\< #\= #\> #\? #\^ #\_ #\~ #\@)
	 #t)
	(else #f))))

(define (subsequent? char)
  (or (initial? char)
      (char<=? #\0 char #\9)
      (case char
	((#\+ #\- #\.)
	 #t)
	(else
	 #f))))

(define (sign? char)
  (or (char=? char #\+)
      (char=? char #\-)))

(define (sign-subsequent? char)
  (or (initial? char) (sign? char)))

(define (dot-subsequent? char)
  (or (sign-subsequent? char)
      (char=? char #\.)))

(define (hex-digit char)
  (cond
   ((eof-object? char)
    #f)
   ((char<=? #\0 char #\9)
    (- (char->integer char) #x30))
   ((char<=? #\A char #\F)
    (- (char->integer char) #x37))
   ((char<=? #\a char #\f)
    (- (char->integer char) #x57))
   (else
    #f)))

(define (hex-scalar-value token)
  (and-let*
      (((> (string-length token) 0))
       (char (string-ref token 0))
       ((or (char=? char #\x) (char=? char #\X))))	  
    (let loop ((value #f) (digits (cdr (string->list token))))
      (cond
       ((null? digits)
	value)
       ((hex-digit (car digits))
	=> (lambda (digit)
	     (loop (+ (* 16 (or value 0)) digit) (cdr digits))))
       (else
	#f)))))

(define (unwrap-tail syntax)
  (let ((datum (unwrap-syntax syntax)))
    (if (or (null? datum) (pair? datum))
	datum
	syntax)))

(define (make-position line column) (vector line column))

(define (position-line position) (vector-ref position 0))

(define (position-column position) (vector-ref position 1))

(define (make-label) (vector #f (list '())))

(define (label-syntax label) (vector-ref label 0))

(define (label-references label) (car (vector-ref label 1)))

(define (label-set-syntax! label syntax) (vector-set! label 0 syntax))

(define (label-add-reference! label reference)
  (let ((reference** (vector-ref label 1)))
    (set-car! reference** (cons reference (car reference**)))))

(define (label-delete-references! label) (vector-set! label 1 #f))

(define (label-add-references! label references)
  (vector-set! label 1 (cons references (vector-ref label 1))))

(define (label-for-each-reference proc label)
  (for-each
   (lambda (references)
     (for-each proc references))
   (vector-ref label 1)))

(define label-comparator
  (make-comparator exact-integer? = < #f))

;;> \procedure{(read-file context pathname ci?)}

;;> Returns the list of syntax objects parsed from reading all external
;;> representations in the file \var{pathname} with case-insensitivity flag set to
;;> \var{ci?}.  \var{Context} is used for syntax error objects when raised.

(define (read-file context pathname ci?)
  (assume (or (not context) (syntax? context)))
  (assume (string? pathname))
  (assume (boolean? ci?))
  ((current-file-reader) context pathname ci?))

;;> \procedure{(read-files-reverse context filenames body ci?)}

;;> Reads the list of syntax objects parsed from reading all external
;;> representations in the files \var{filename}s in the vinicity of
;;> the syntax object \var{context} with case-insensitivity flag set
;;> to \var{ci?}.  Prepends this list in reverse order to the list
;;> \var{body} and returns the results.  The list \var{filenames} is a
;;> list of syntax objects each wrapping a string.  A syntax error is
;;> raised if any syntax object in \var{filenames} does not wrap a
;;> string.

(define (read-files-reverse context filenames body ci?)
  (fold (lambda (filename-syntax body)
	  (let ((filename (unwrap-syntax filename-syntax)))
	    (cond
	     ((string? filename)
	      (append-reverse (read-file context
					 (in-vicinity (syntax->vicinity context)
						      filename)
					 ci?)
			      body))
	     (else
	      (raise-syntax-error filename-syntax "invalid type, expected string")
	      body))))
	body filenames))

;;;; Parameter objects

;;> \procedure{(current-file-reader)}

;;> By rebinding this parameter object to a custom procedure, the
;;> behaviour of \scheme{read-file} and \scheme{read-files-reverse}
;;> can be customized.  It is expected that the new binding is a
;;> procedure with the same signature and semantics as
;;> \scheme{read-file}.

(define current-file-reader
  (make-parameter
   (lambda (context pathname ci?)
     (guard
	 (condition ((file-error? condition)
		     (raise-syntax-error context "unable to open input file ‘~a’" pathname)))     
       (call-with-input-file pathname
	 (lambda (port)
	   (let ((source-port (make-source-port port pathname ci?)))
	     (unfold eof-object?
		     values
		     (lambda (value) (read-syntax source-port))
		     (read-syntax source-port)))))))))
