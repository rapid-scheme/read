;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Scheme reader with source-location information.

(define-library (rapid read)
  (export make-source-port
	  source-port?
	  source-port-ci?
	  source-port-set-ci!
	  source-port-fold-case!
	  source-port-no-fold-case!
	  read-syntax
	  read-file
	  read-files-reverse
	  current-file-reader)
  (import (scheme base)
	  (scheme char)
	  (scheme file)
	  (rapid assume)
	  (rapid and-let)
	  (rapid receive)
	  (rapid list)
	  (rapid format)
	  (rapid vicinity)
	  (rapid comparator)
	  (rapid mapping)
          (rapid syntax))
  (include "read.scm"))
